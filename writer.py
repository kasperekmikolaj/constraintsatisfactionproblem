from copy import deepcopy
from typing import List

import numpy as np

from jolka_problem import JolkaVariable, get_jolka
from sudoku_problem import SudokuVariable


class DataObject:

    def __init__(self, first_found_time,
                 nodes_visited_until_first_solution,
                 recurrence_number_until_first_solution,
                 method_time,
                 all_nodes_visited,
                 all_recurrence,
                 number_of_results,
                 results):
        self.first_found_time = first_found_time
        self.nodes_visited_until_first_solution = nodes_visited_until_first_solution
        self.recurrence_number_until_first_solution = recurrence_number_until_first_solution
        self.method_time = method_time
        self.all_nodes_visited = all_nodes_visited
        self.all_recurrence = all_recurrence
        self.number_of_results = number_of_results
        self.results = results


def write_sudoku_results(data: DataObject, sudoku_number: int, folder_path: str):
    file = open(folder_path + '/sudoku_results' + str(sudoku_number) + '.csv', 'w')

    file.write('sudoku number ' + str(sudoku_number) + '\n')
    file.write(str(data.first_found_time) + ';' + ' <- first_found_time\n')
    file.write(str(data.nodes_visited_until_first_solution) + ';' + ' <- nodes_visited_until_first_solution\n')
    file.write(
        str(data.recurrence_number_until_first_solution) + ';' + ' <- recurrence_number_until_first_solution\n')
    file.write(str(data.method_time) + ';' + ' <- method_time\n')
    file.write(str(data.all_nodes_visited) + ';' + ' <- all_nodes_visited\n')
    file.write(str(data.all_recurrence) + ';' + ' <- all_recurrence\n')
    file.write(str(data.number_of_results) + ';' + ' <- number_of_results\n')

    for index, result in enumerate(data.results):
        file.write('solution number ' + str(index + 1) + '\n')
        matrix = np.reshape(result, (9, 9))
        for row in matrix:
            for item in row:
                file.write(str(item.value) + ';')
            file.write('\n')
        file.write('\n')
    file.close()


def write_jolka_results(data: DataObject, jolka_number: int, starting_jolka, folder_path: str):
    file = open(folder_path + '/jolka_results' + str(jolka_number) + '.csv', 'w')

    file.write('jolka number ' + str(jolka_number) + '\n')
    file.write(str(data.first_found_time) + ';' + ' <- first_found_time\n')
    file.write(str(data.nodes_visited_until_first_solution) + ';' + ' <- nodes_visited_until_first_solution\n')
    file.write(
        str(data.recurrence_number_until_first_solution) + ';' + ' <- recurrence_number_until_first_solution\n')
    file.write(str(data.method_time) + ';' + ' <- method_time\n')
    file.write(str(data.all_nodes_visited) + ';' + ' <- all_nodes_visited\n')
    file.write(str(data.all_recurrence) + ';' + ' <- all_recurrence\n')
    file.write(str(data.number_of_results) + ';' + ' <- number_of_results\n')

    for index, result in enumerate(data.results):
        file.write('solution number ' + str(index + 1) + '\n')
        jolka = get_jolka(result, starting_jolka)
        for row in jolka:
            for char in row:
                file.write(char + ';')
            file.write('\n')
        file.write('\n')
    file.close()


def write_test_sudoku(result: List[SudokuVariable]):
    file = open('results/test_sudoku' + '.csv', 'w')
    matrix = np.reshape(result, (9, 9))
    for row in matrix:
        for item in row:
            file.write(str(item.value) + ';')
        file.write('\n')
    file.write('\n')
    file.close()


def write_test_jolka(result: List[JolkaVariable], starting_jolka):
    file = open('results/test_jolka' + '.csv', 'w')
    jolka = get_jolka(result, starting_jolka)
    for row in jolka:
        for char in row:
            file.write(char + ';')
        file.write('\n')
    file.write('\n')
    file.close()
