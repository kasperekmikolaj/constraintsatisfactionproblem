from random import shuffle

import numpy as np


class SudokuVariable:

    def __init__(self, possible_values: list, value: int, is_const: bool, row_index: int, column_index: int,
                 random_method=False):
        if random_method:
            shuffle(possible_values)
        self.possible_values = possible_values
        self.column_index = column_index
        self.row_index = row_index
        self.is_const = is_const
        self.value = value
        self.already_checked_values = []
        self.default_value = 0

        self.blocked_values = []

    def __repr__(self):
        return str(self) + "'"

    def __str__(self):
        return str(self.value)


class SudokuProblem:

    def __init__(self, sudoku_data: str, difficulty: int, sud_id: int, random_method=False):
        self.difficulty = difficulty
        self.id = sud_id
        if random_method:
            self.starting_sudoku, as_list = generate_sudoku(sudoku_data, random_method)
        else:
            self.starting_sudoku, as_list = generate_sudoku(sudoku_data)
        self.starting_sudoku_list = [item for sub_list in as_list for item in sub_list]


def generate_sudoku(sudoku_data, random_method=False) -> (np.ndarray, list):
    possible_values = [i for i in range(1, 10)]
    array = [[SudokuVariable([int(char)], int(char), True, row_index, column_index) if char.isdigit()
              else SudokuVariable(possible_values, 0, False, row_index, column_index, random_method) for
              column_index, char in
              enumerate(sudoku_data[row_index * 9: row_index * 9 + 9])] for row_index in range(9)]
    return np.array(array), array


def is_valid(checking_variable: SudokuVariable, checking_value: int, current_sudoku: list) -> bool:
    temp_current_sudoku = np.array(
        [[current_sudoku[row * 9 + column].value for column in range(9)] for row in range(9)])
    temp_current_sudoku[checking_variable.row_index, checking_variable.column_index] = 0
    if checking_value in temp_current_sudoku[checking_variable.row_index]:
        return False
    if checking_value in temp_current_sudoku[:, checking_variable.column_index]:
        return False
    if checking_value in get_square(checking_variable.row_index, checking_variable.column_index,
                                    temp_current_sudoku):
        return False
    return True


def get_square(row_index: int, column_index: int, sudoku: np.ndarray):
    square_horizontal_number = column_index // 3
    square_vertical_number = row_index // 3
    square = [sudoku[square_vertical_number * 3 + i][square_horizontal_number * 3 + j] for i in range(3) for j in
              range(3)]
    return square


def remove_blockade(value_to_remove, variable: SudokuVariable, current_result):
    square_horizontal_number = variable.column_index // 3
    square_vertical_number = variable.row_index // 3
    for r in range(3):
        for c in range(3):
            index = (square_vertical_number * 27) + r * 9 + square_horizontal_number * 3 + c
            var = current_result[index]
            if value_to_remove in var.blocked_values:
                var.blocked_values.remove(value_to_remove)
    for var in current_result[variable.row_index * 9:variable.row_index * 9 + 9]:
        if value_to_remove in var.blocked_values:
            var.blocked_values.remove(value_to_remove)
    for row_index in range(9):
        var = current_result[row_index * 9 + variable.column_index]
        if value_to_remove in var.blocked_values:
            var.blocked_values.remove(value_to_remove)


def add_blockade(value_to_add, variable: SudokuVariable, current_result) -> bool:
    if value_to_add == 0:
        return False
    square_horizontal_number = variable.column_index // 3
    square_vertical_number = variable.row_index // 3
    for r in range(3):
        for c in range(3):
            index = (square_vertical_number * 27) + r * 9 + square_horizontal_number * 3 + c
            var = current_result[index]
            if not var.is_const and value_to_add not in var.blocked_values:
                var.blocked_values.append(value_to_add)
                if len(var.blocked_values) == len(var.possible_values) and var.value == 0 and not var == variable:
                    return True  # if no possible solution
    for var in current_result[variable.row_index * 9:variable.row_index * 9 + 9]:
        if not var.is_const and value_to_add not in var.blocked_values:
            var.blocked_values.append(value_to_add)
            if len(var.blocked_values) == len(var.possible_values) and var.value == 0 and not var == variable:
                return True  # if no possible solution
    for row_index in range(9):
        var = current_result[row_index * 9 + variable.column_index]
        if not var.is_const and value_to_add not in var.blocked_values:
            var.blocked_values.append(value_to_add)
            if len(var.blocked_values) == len(var.possible_values) and var.value == 0 and not var == variable:
                return True  # if no possible solution
    return False













    # # printing matrix
    # for row in range(9):
    #     t = ''
    #     for column in range(9):
    #         t += str(current_sudoku[row][column])
    #     print(t)
