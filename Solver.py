import time
from copy import deepcopy
from random import shuffle

from jolka_problem import JolkaProblem
from reader import read_jolka_data
from writer import DataObject
import numpy as np

# only algorithm
# def solve(starting_variable_list, is_valid_function):
#     current_result = deepcopy(starting_variable_list)
#     variable_index = 0
#     value_index = 0
#     finished = False  # to find all solutions
#     results = []
#     while not finished:
#         while variable_index < len(current_result):
#             variable = current_result[variable_index]
#             possible_values_for_variable = current_result[variable_index].possible_values
#             if value_index == len(possible_values_for_variable):  # 9
#                 variable.already_checked_values = []
#                 if not variable.is_const:
#                     variable.value = variable.default_value
#                 previous_variable = current_result[variable_index - 1]
#                 previous_variable.already_checked_values.append(previous_variable.value)
#                 variable_index -= 1
#                 value_index = len(previous_variable.already_checked_values)
#                 if variable_index < 0:  # 12
#                     finished = True
#                     break
#             else:
#                 next_value = possible_values_for_variable[value_index]  # 18
#                 is_allowed = is_valid_function(variable, next_value, current_result)
#                 if is_allowed:  # 20
#                     variable.value = next_value
#                     variable_index += 1
#                     value_index = 0
#                 else:  # 22
#                     variable.already_checked_values.append(next_value)
#                     value_index += 1
#         if not finished:  # to find all solutions
#             results.append(get_current_list(current_result))
#             variable = current_result[len(current_result) - 1]
#             variable.already_checked_values = []
#             if not variable.is_const:
#                 variable.value = variable.default_value
#             previous_variable = current_result[len(current_result) - 2]
#             previous_variable.already_checked_values.append(previous_variable.value)
#             variable_index = len(current_result) - 2
#             value_index = len(previous_variable.already_checked_values)
#             print(len(results))



# algorithm with time calculation
def solve(starting_variable_list, is_valid_function, random_method=False):
    # data only
    start_time = time.time()
    nodes_visited_until_first_solution = 0
    all_nodes_visited = 0
    recurrence_number_until_first_solution = 0
    all_recurrence = 0
    first_found_time = 0
    found_first = False

    finished = False  # to find all solutions

    variable_index_list = [index for index in range(len(starting_variable_list))]  # for random only
    shuffle(variable_index_list)  # for random only
    current_result = deepcopy(starting_variable_list)
    variable_index = 0
    value_index = 0
    results = []
    while not finished:
        while variable_index < len(current_result):
            if random_method:  # random
                variable = current_result[variable_index_list[variable_index]]  # random
            else:  # random
                variable = current_result[variable_index]
            possible_values_for_variable = variable.possible_values
            if value_index == len(possible_values_for_variable):  # 9
                variable.already_checked_values = []
                if random_method:  # random
                    shuffle(variable.possible_values)  # random
                if not variable.is_const:
                    variable.value = variable.default_value
                if random_method:  # random
                    previous_variable = current_result[variable_index_list[variable_index - 1]]  # random
                else:  # random
                    previous_variable = current_result[variable_index - 1]
                previous_variable.already_checked_values.append(previous_variable.value)
                variable_index -= 1
                value_index = len(previous_variable.already_checked_values)
                if not found_first:  # data only
                    recurrence_number_until_first_solution += 1  # data only
                all_recurrence += 1  # data only
                if variable_index < 0:  # 12
                    finished = True
                    break
            else:
                if found_first:  # data only
                    nodes_visited_until_first_solution += 1  # data only
                all_nodes_visited += 1  # data only
                next_value = possible_values_for_variable[value_index]  # 18
                is_allowed = is_valid_function(variable, next_value, current_result)
                if is_allowed:  # 20
                    variable.value = next_value
                    variable_index += 1
                    value_index = 0
                else:  # 22
                    variable.already_checked_values.append(next_value)
                    value_index += 1
        if not finished:  # to find all solutions
            results.append(deepcopy(current_result))
            if len(results) == 1:  # data only
                found_first = True  # data only
                first_found_time = time.time() - start_time  # data only
            if random_method:  # random
                index = variable_index_list[len(current_result) - 1]  # random
                variable = current_result[index]  # random
            else:  # random
                variable = current_result[len(current_result) - 1]
            variable.already_checked_values = []
            if not variable.is_const:
                variable.value = variable.default_value
            if random_method:  # random
                index = variable_index_list[len(current_result) - 2]  # random
                previous_variable = current_result[index]  # random
            else:  # random
                previous_variable = current_result[len(current_result) - 2]
            previous_variable.already_checked_values.append(previous_variable.value)
            variable_index = len(current_result) - 2
            value_index = len(previous_variable.already_checked_values)
            print(len(results))
    number_of_results = len(results)
    method_time = time.time() - start_time  # data only
    data_object = DataObject(first_found_time, nodes_visited_until_first_solution, recurrence_number_until_first_solution, \
           method_time, all_nodes_visited, all_recurrence, number_of_results, results)
    return data_object



