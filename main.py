from random import shuffle

import Solver
from forward_checking_solver import forward_solve
from jolka_problem import JolkaProblem
from reader import read_sudoku_data, read_jolka_data
import sudoku_problem as Sudoku
import jolka_problem as Jolka
from writer import write_sudoku_results, write_jolka_results, write_test_sudoku, write_test_jolka


if __name__ == '__main__':

    sudoku_number = 40

    jolka_number = 2
    board, words = read_jolka_data(jolka_number)

    # ##  @@@@@@@@@@@@@@@@@@@      Part 1      @@@@@@@@@@@@@@@@
    sudoku_lst = read_sudoku_data()
    sudoku_problem = sudoku_lst[sudoku_number]
    data_object = Solver.solve(sudoku_problem.starting_sudoku_list, Sudoku.is_valid)
    write_sudoku_results(data_object, sudoku_number, 'results/backward_results')

    # jolka = JolkaProblem(board, words)
    # data_object = Solver.solve(jolka.variables, jolka.is_valid, True)
    # write_jolka_results(data_object, jolka_number, jolka.starting_jolka, 'results/backward_results')
    ###  @@@@@@@@@@@@@@@@@@@      Part 1      @@@@@@@@@@@@@@@@
    #
    #
    #
    #
    #
    ###  @@@@@@@@@@@@@@@@@@@      Part 2      @@@@@@@@@@@@@@@@
    # sudoku_lst = read_sudoku_data()
    # sudoku_problem = sudoku_lst[sudoku_number]
    # result = forward_solve(sudoku_problem.starting_sudoku_list, Sudoku.is_valid, Sudoku.remove_blockade, Sudoku. add_blockade)
    # write_sudoku_results(result, sudoku_number, 'results/forward_results')
    #
    # jolka = JolkaProblem(board, words)
    # result = forward_solve(jolka.variables, jolka.is_valid, Jolka.remove_blockade, Jolka.add_blockade)
    # write_jolka_results(result, jolka_number, jolka.starting_jolka, 'results/forward_results')
    ###  @@@@@@@@@@@@@@@@@@@      Part 2      @@@@@@@@@@@@@@@@




















