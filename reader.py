from math import floor

from sudoku_problem import SudokuProblem


def read_sudoku_data(random_method=False):
    file = open('ai-lab2-2020-dane/Sudoku.csv')
    sudoku_list = []
    for line in file.readlines():
        if line[0].isdigit():
            sud_id, difficulty, sudoku, _ = line.split(sep=';')
            sudoku_list.append(SudokuProblem(sudoku, int(floor(float(difficulty))), int(sud_id), random_method))
    file.close()
    return sudoku_list


def read_jolka_data(file_number: int):
    puzzle_file = open('ai-lab2-2020-dane/Jolka/puzzle' + str(file_number))
    word_file = open('ai-lab2-2020-dane/Jolka/words' + str(file_number))
    jolka_board = puzzle_file.read()
    jolka_board = jolka_board[0:len(jolka_board) - 1]
    words = []
    for line in word_file.readlines():
        words.append(line[0:len(line) - 1])
    word_file.close()
    puzzle_file.close()
    return jolka_board, words









