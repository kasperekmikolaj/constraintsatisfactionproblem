import time
from copy import deepcopy
from random import shuffle
from typing import List

import numpy as np


class Point:

    def __init__(self, row_index: int, column_index: int):
        self.row_index = row_index
        self.column_index = column_index


class JolkaVariable:

    def __init__(self, start_point: Point, end_point: Point, word_list: List[str], jolka_problem):
        self.start_point = start_point
        self.end_point = end_point
        self.is_row = True if start_point.row_index == end_point.row_index else False
        self.length = self._calculate_length()
        self.possible_values = self._generate_possible_values(word_list)
        temp = ''
        for i in range(self.length):
            temp += '_'
        self.default_value = temp
        self._value = temp
        self._already_checked_values = []
        self.is_const = False
        self.is_clear = True
        self.blocked_values = []
        self.jolka_problem = jolka_problem

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self.is_clear = True if value == self.default_value else False
        self._value = value

    @property
    def already_checked_values(self):
        return self._already_checked_values

    @already_checked_values.setter
    def already_checked_values(self, new_list: List):
        if not new_list:
            self.value = self.default_value
        self._already_checked_values = new_list

    def _calculate_length(self) -> int:
        if self.is_row:
            return self.end_point.column_index - self.start_point.column_index + 1
        else:
            return self.end_point.row_index - self.start_point.row_index + 1

    def _generate_possible_values(self, word_list: List[str]) -> List[str]:
        return [word for word in word_list if len(word) == self.length]

    def get_value_at(self, index: int) -> str:
        return self.value[index]

    def __repr__(self):
        return str(self) + "'"

    def __str__(self):
        return self.value


class JolkaProblem:

    def __init__(self, board: str, word_list: List[str], random_method=False):
        self.variables = []
        rotated_board = self._get_rotated_board(board)
        if random_method:
            shuffle(word_list)
        self._generate_variables(board, word_list)
        self._generate_variables(rotated_board, word_list, False)
        self.starting_jolka = self._create_starting_jolka(board)

    def _generate_variables(self, board: str, word_list: List[str], gen_rows=True):
        min_word_length = min([len(word) for word in word_list])
        rows = board.split('\n')
        for row_index, row in enumerate(rows):
            row += '#'  # to identify end of line
            search_from = 0
            try:
                while True:
                    starting_column_index = row.index('_', search_from)
                    search_from = starting_column_index + 1
                    ending_column_index = row.index('#', search_from) - 1
                    search_from = ending_column_index + 1
                    place_length = ending_column_index - starting_column_index + 1
                    if place_length >= min_word_length:
                        if gen_rows:
                            start_point = Point(row_index, starting_column_index)
                            end_point = Point(row_index, ending_column_index)
                            self.variables.append(JolkaVariable(start_point, end_point, word_list, self))
                        else:
                            start_point = Point(starting_column_index, row_index)
                            end_point = Point(ending_column_index,  row_index)
                            self.variables.append(JolkaVariable(start_point, end_point, word_list, self))
            except ValueError:
                pass

    def _get_rotated_board(self, board):
        rotated_board = ''
        rows = board.split('\n')
        for column_index in range(len(rows[0])):
            for row in rows:
                rotated_board += row[column_index]
            rotated_board += '\n'
        return rotated_board[0:len(rotated_board) - 1]

    def _create_starting_jolka(self, board: str) -> np.array:
        rows = board.split('\n')
        return np.array([[char for char in row] for row in rows])

    def is_valid(self, variable: JolkaVariable, next_value: str, current_result: List[JolkaVariable]) -> bool:
        variable.value = variable.default_value
        already_used_words = [variable.value for variable in current_result]
        if next_value in already_used_words:
            return False
        current_jolka = get_jolka(current_result, self.starting_jolka)
        if variable.is_row:
            row_index = variable.start_point.row_index
            current_column_index = variable.start_point.column_index
            for char in next_value:
                checking_char = current_jolka[row_index, current_column_index]
                current_column_index += 1
                if checking_char != '_' and checking_char != char:
                    return False
        else:
            column_index = variable.start_point.column_index
            current_row_index = variable.start_point.row_index
            for char in next_value:
                checking_char = current_jolka[current_row_index, column_index]
                current_row_index += 1
                if checking_char != '_' and checking_char != char:
                    return False
        return True


def get_jolka(current_result: List[JolkaVariable], starting_jolka: np.array) -> np.array:
    result = deepcopy(starting_jolka)
    for i, variable in enumerate(current_result):
        if not variable.is_clear:
            if variable.is_row:
                row_index = variable.start_point.row_index
                start_column_index = variable.start_point.column_index
                for char in variable.value:
                    result[row_index, start_column_index] = char
                    start_column_index += 1
            else:
                column_index = variable.start_point.column_index
                start_row_index = variable.start_point.row_index
                for char in variable.value:
                    result[start_row_index, column_index] = char
                    start_row_index += 1
    return result


def remove_blockade(value_to_remove, start_variable: JolkaVariable, current_result):
    if value_to_remove == start_variable.default_value:
        return
    start_index = current_result.index(start_variable) + 1
    for index_of_var_to_reset in range(start_index, len(current_result)):
        var_to_reset_blocked_value = current_result[index_of_var_to_reset]
        if value_to_remove in var_to_reset_blocked_value.blocked_values:
            var_to_reset_blocked_value.blocked_values.remove(value_to_remove)


def add_blockade(value_to_add: str, start_variable: JolkaVariable, current_result: List[JolkaVariable]) -> bool:
    c_result_copy = deepcopy(current_result)
    var_index = current_result.index(start_variable)
    c_var_copy = c_result_copy[var_index]
    c_var_copy.value = value_to_add
    start_index = var_index + 1
    jolka_with_new_value = get_jolka(c_result_copy, c_var_copy.jolka_problem.starting_jolka)
    for index in range(start_index, len(current_result)):
        var_to_update = current_result[index]
        if value_to_add in var_to_update.possible_values:
            var_to_update.blocked_values.append(value_to_add)
            # check if all variable can pick any value
            var_to_update_copy = c_result_copy[index]
            for value in var_to_update_copy.possible_values:
                if value not in var_to_update_copy.blocked_values:
                    if var_to_update_copy.is_row:
                        row_index = var_to_update_copy.start_point.row_index
                        current_column_index = var_to_update_copy.start_point.column_index
                        for char in value:
                            checking_char = jolka_with_new_value[row_index, current_column_index]
                            current_column_index += 1
                            if checking_char != '_' and checking_char != char:
                                var_to_update_copy.blocked_values.append(value)
                                break
                    else:
                        column_index = var_to_update_copy.start_point.column_index
                        current_row_index = var_to_update_copy.start_point.row_index
                        for char in value:
                            checking_char = jolka_with_new_value[current_row_index, column_index]
                            current_row_index += 1
                            if checking_char != '_' and checking_char != char:
                                var_to_update_copy.blocked_values.append(value)
                                break
                    if len(var_to_update_copy.blocked_values) == len(var_to_update_copy.possible_values):
                        return True
    return False
































