import time
from copy import deepcopy

import numpy as np

from sudoku_problem import get_square, SudokuVariable


# def forward_solve(starting_variable_list, is_valid_function, remove_blockade, add_blockade):
#     current_result = deepcopy(starting_variable_list)
#     variable_index = 0
#     value_index = 0
#     while variable_index < len(current_result):
#         variable = current_result[variable_index]
#         possible_values_for_variable = current_result[variable_index].possible_values
#         if value_index == len(possible_values_for_variable):  # 9
#             variable.already_checked_values = []
#             if not variable.is_const:
#                 #  remove blockade made by current variable
#                 remove_blockade(variable.value, variable, current_result)
#                 variable.value = variable.default_value
#             previous_variable = current_result[variable_index - 1]
#             previous_variable.already_checked_values.append(previous_variable.value)
#             #  remove blockade made by previous variable
#             remove_blockade(previous_variable.value, previous_variable, current_result)
#             variable_index -= 1
#             value_index = len(previous_variable.already_checked_values)
#             if variable_index < 0:  # 12
#                 finished = True
#                 break
#         else:
#             next_value = possible_values_for_variable[value_index]  # 18
#             if next_value not in variable.blocked_values:
#                 is_allowed = is_valid_function(variable, next_value, current_result)
#                 if is_allowed:  # 20
#                     # remove old blockade - new value for variable = different blocking value
#                     remove_blockade(variable.value, variable, current_result)
#                     #  add new blocking value -> new variable value
#                     is_blockade = add_blockade(next_value, variable, current_result)
#                     if is_blockade:
#                         #  if new blockade makes it impossible to find solution reverse it
#                         remove_blockade(next_value, variable, current_result)
#                         #  and add previous blocking value
#                         add_blockade(variable.value, variable, current_result)
#                         variable.already_checked_values.append(next_value)
#                         value_index += 1
#                     else:
#                         variable.value = next_value
#                         variable_index += 1
#                         value_index = 0
#                 else:  # 22
#                     variable.already_checked_values.append(next_value)
#                     value_index += 1
#             else:  # 22
#                 variable.already_checked_values.append(next_value)
#                 value_index += 1
#     return current_result
from writer import DataObject


def forward_solve(starting_variable_list, is_valid_function, remove_blockade, add_blockade):
    # data only
    start_time = time.time()
    nodes_visited_until_first_solution = 0
    all_nodes_visited = 0
    recurrence_number_until_first_solution = 0
    all_recurrence = 0
    first_found_time = 0
    found_first = False

    finished = False  # to find all solutions

    current_result = deepcopy(starting_variable_list)
    variable_index = 0
    value_index = 0
    results = []
    while not finished:
        while variable_index < len(current_result):
            variable = current_result[variable_index]
            possible_values_for_variable = current_result[variable_index].possible_values
            if value_index == len(possible_values_for_variable):  # 9
                variable.already_checked_values = []
                if not variable.is_const:
                    #  remove blockade made by current variable
                    remove_blockade(variable.value, variable, current_result)
                    variable.value = variable.default_value
                previous_variable = current_result[variable_index - 1]
                previous_variable.already_checked_values.append(previous_variable.value)
                #  remove blockade made by previous variable
                remove_blockade(previous_variable.value, previous_variable, current_result)
                variable_index -= 1
                value_index = len(previous_variable.already_checked_values)
                if not found_first:  # data only
                    recurrence_number_until_first_solution += 1  # data only
                all_recurrence += 1  # data only
                if variable_index < 0:  # 12
                    finished = True
                    break
            else:
                next_value = possible_values_for_variable[value_index]  # 18
                if next_value not in variable.blocked_values:
                    if found_first:  # data only
                        nodes_visited_until_first_solution += 1  # data only
                    all_nodes_visited += 1  # data only
                    is_allowed = is_valid_function(variable, next_value, current_result)
                    if is_allowed:  # 20
                        # remove old blockade - new value for variable = different blocking value
                        remove_blockade(variable.value, variable, current_result)
                        #  add new blocking value -> new variable value
                        is_blockade = add_blockade(next_value, variable, current_result)
                        if is_blockade:
                            #  if new blockade makes it impossible to find solution reverse it
                            remove_blockade(next_value, variable, current_result)
                            #  and add previous blocking value
                            add_blockade(variable.value, variable, current_result)
                            variable.already_checked_values.append(next_value)
                            value_index += 1
                        else:
                            variable.value = next_value
                            variable_index += 1
                            value_index = 0
                    else:  # 22
                        variable.already_checked_values.append(next_value)
                        value_index += 1
                else:  # 22
                    variable.already_checked_values.append(next_value)
                    value_index += 1
        if not finished:  # to find all solutions
            results.append(deepcopy(current_result))
            if len(results) == 1:  # data only
                found_first = True  # data only
                first_found_time = time.time() - start_time  # data only
            variable = current_result[len(current_result) - 1]
            variable.already_checked_values = []
            if not variable.is_const:
                remove_blockade(variable.value, variable, current_result)
                variable.value = variable.default_value
            previous_variable = current_result[len(current_result) - 2]
            previous_variable.already_checked_values.append(previous_variable.value)
            variable_index = len(current_result) - 2
            value_index = len(previous_variable.already_checked_values)
            print(len(results))
    number_of_results = len(results)
    method_time = time.time() - start_time  # data only
    data_object = DataObject(first_found_time, nodes_visited_until_first_solution, recurrence_number_until_first_solution, \
           method_time, all_nodes_visited, all_recurrence, number_of_results, results)
    return data_object



